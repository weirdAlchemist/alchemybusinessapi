using System;
using Xunit;

namespace tests
{
    public class LevenshteinTests
    {
        [Fact]
        public void OneOnOneEqualTest() {

        }

        [Fact]
        public void OneInManyEqualTest() {

        }

        [Fact]
        public void ManyInManyEqualTest() {

        }

        [Fact]
        public void OneOnOneCloseTest() {

        }

        [Fact]
        public void OneInManyCloseTest() {

        }

        [Fact]
        public void ManyInManyCloseTest() {

        }

        [Fact]
        public void OneOnOneDistantTest() {

        }

        [Fact]
        public void OneInManyDistantTest() {

        }

        [Fact]
        public void ManyInManyDistantTest() {

        }
    }
}